package one.spo.coding.web;

import com.google.gson.Gson;
import one.spo.coding.domain.CapacityPairResponse;
import one.spo.coding.domain.WorkforceRequest;
import one.spo.coding.service.PartnerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * User: raychenon
 */
@RunWith(SpringRunner.class)
@WebMvcTest(PartnerController.class)
public class PartnerControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private PartnerService partnerService;
    
    @Test
    public void getIndex() throws Exception{
        this.mvc.perform(get("/")).andExpect(status().isOk());
    }


    @Test
    public void getCapacityWhenPostingShouldReturnResponse() throws Exception {
        List<Integer> rooms = Arrays.asList(1);
        Integer senior = 5;
        Integer junior = 2;
        given(this.partnerService.optimizeCapacity(rooms,senior,junior))
                .willReturn(Arrays.asList(new CapacityPairResponse(1,0)));

        WorkforceRequest request = new WorkforceRequest(rooms,senior,junior);
        Gson gson = new Gson();
        String json = gson.toJson(request);
        
        this.mvc.perform(post("/workforce")
                .content(json)
        ).andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"));
    }
    

    @Test
    public void getCapacityWhenRequestingQueryParamsShouldReturnResponse() throws Exception {
        List<Integer> rooms = Arrays.asList(1);
        Integer senior = 5;
        Integer junior = 2;
        given(this.partnerService.optimizeCapacity(rooms,senior,junior))
                .willReturn(Arrays.asList(new CapacityPairResponse(1,0)));

        this.mvc.perform(get("/optimization")
                .param("rooms", "1")
                .param("senior", senior.toString())
                .param("junior",junior.toString())
            ).andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"));
    }
}
