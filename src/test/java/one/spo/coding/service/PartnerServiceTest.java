package one.spo.coding.service;

import one.spo.coding.domain.CapacityPairResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * User: raychenon
 */
@RunWith(SpringRunner.class)
@RestClientTest(PartnerService.class)
public class PartnerServiceTest {

    @Autowired
    private PartnerService service;

    @Test
    public void optimizeCapacityShouldRespectRules(){
        assertRulesRespected(Arrays.asList(35,21,17),10,6);
        assertRulesRespected(Arrays.asList(24,28),11,6);
    }

    @Test
    public void optimizeCapacityShouldBeEqualToExpectedOutput_Case1(){

        List<CapacityPairResponse> response = service.optimizeCapacity(Arrays.asList(35,21,17),10,6);

        assertEquals(3,response.get(0).getSenior());
        assertEquals(1,response.get(0).getJunior());

        assertEquals(1,response.get(1).getSenior());
        assertEquals(2,response.get(1).getJunior());

        assertEquals(2,response.get(2).getSenior());
        assertEquals(0,response.get(2).getJunior());
    }

    @Test
    public void optimizeCapacityShouldBeEqualToExpectedOutput_Case2(){

        List<CapacityPairResponse> response = service.optimizeCapacity(Arrays.asList(24,28),11,6);

        assertEquals(2,response.get(0).getSenior());
        assertEquals(1,response.get(0).getJunior());

        assertEquals(2,response.get(1).getSenior());
        assertEquals(1,response.get(1).getJunior());
    }


    private void assertRulesRespected(List<Integer> structures, int senior,int junior){
        List<CapacityPairResponse> items = service.optimizeCapacity(structures,senior,junior);
        
        // the number of pair Senior/Junior is qual to the number of structure
        assertEquals(items.size(), structures.size());

        // there always needs to be at least one Senior cleaner at every structure to lead the juniors
        for(CapacityPairResponse pair: items){
            assertThat(pair.getSenior() > 0);
        }
    }

}
