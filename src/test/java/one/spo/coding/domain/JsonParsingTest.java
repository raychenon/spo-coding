package one.spo.coding.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * User: raychenon
 */
@RunWith(SpringRunner.class)
public class JsonParsingTest {

    @Test
    public void parseJSONRequest() {

        String json = "{ rooms: [35, 21, 17], senior: 10, junior: 6 }";
        WorkforceRequest request = GsonUtils.fromRequest(json);
        assertThat(request.getSenior()).isEqualTo(10);
        assertThat(request.getJunior()).isEqualTo(6);
        assertThat(request.getRooms()).isEqualTo(Arrays.asList(35,21,17));
    }

    @Test
    public void parseWhenaFieldIsNullShouldBeZero() {
        String json = "{ rooms: [35, 21, 17], junior: 6 }";
        WorkforceRequest request = GsonUtils.fromRequest(json);
        assertThat(request.getSenior()).isEqualTo(0);
        assertThat(request.getJunior()).isEqualTo(6);
        assertThat(request.getRooms()).isEqualTo(Arrays.asList(35,21,17));
    }

    @Test
    public void parseSingleResponse() {

        List<CapacityPairResponse> single = Arrays.asList(new CapacityPairResponse(2,1));
        String json = GsonUtils.convertToJson(single);
        assertThat("[{\"senior\":2,\"junior\":1}]").isEqualTo(json);
    }

    @Test
    public void parseListResponse() {

        List<CapacityPairResponse> response = Arrays.asList(
                new CapacityPairResponse(1,5),
                new CapacityPairResponse(2,1));
        String json = GsonUtils.convertToJson(response);
        assertThat("[{\"senior\":1,\"junior\":5},{\"senior\":2,\"junior\":1}]").isEqualTo(json);
    }

    @Test
    public void parseEmptyResponse() {

        List<CapacityPairResponse> emptyResponse = Arrays.asList();
        String json = GsonUtils.convertToJson(emptyResponse);
        assertThat("[]").isEqualTo(json);
    }



}
