package one.spo.coding.service;

import one.spo.coding.domain.CapacityPairResponse;
import org.jblas.DoubleMatrix;
import org.springframework.stereotype.Service;

import javax.inject.Singleton;
import java.util.LinkedList;
import java.util.List;

/**
 * User: raychenon
 */
@Singleton
@Service
public class PartnerService implements OptimizationService {

    /**
     * Our partner is free to decide how many Senior and Junior
     * Cleaners are to be sent to clean a structure but there always needs to be at least one Senior cleaner at
     * every structure to lead the juniors. The goal is to minimize overcapacity at every structure.
     *
     * @param rooms
     * @param senior
     * @param junior
     * @return capacity of pair of Senior/Junior cleaner per structure
     */
    @Override
    public List<CapacityPairResponse> optimizeCapacity(List<Integer> rooms, int senior, int junior) {
        List<CapacityPairResponse> accumulatedList = new LinkedList<>();

        for (int nbRooms : rooms) {
            CapacityPairResponse item = OptimizeUtils.optimizePerStructure(nbRooms,senior,junior);

            accumulatedList.add(item);
        }

        return accumulatedList;
    }
    
}
