package one.spo.coding.service;

import one.spo.coding.domain.CapacityPairResponse;

/**
 * User: raychenon
 */
public class OptimizeUtils {

    public static CapacityPairResponse optimizePerStructure(int nbRoomsInStructure, int seniorCapacity, int juniorCapacity) {

        int startSeniors = (int) Math.ceil((double) nbRoomsInStructure / (double) seniorCapacity); // there must be at least 1 senior
        int startJuniors = 0;

        int minOffset = GreatestCommonDivisor(seniorCapacity, juniorCapacity);
        int minStepSize = Math.min(seniorCapacity, juniorCapacity);

        int[] result = exploreParams(Integer.MAX_VALUE, nbRoomsInStructure, minOffset, minStepSize, seniorCapacity, juniorCapacity, startSeniors, startSeniors, startJuniors, startJuniors);

        return new CapacityPairResponse(result[0], result[1]);
    }


    private static int GreatestCommonDivisor(int a, int b) {
        if (b == 0) {
            return a;
        }
        return GreatestCommonDivisor(b, a % b);
    }

    private static int[] exploreParams(int priorDistance, int buildingSize, int minInterval, int minStep, int seniorCapacity, int juniorCapacity, int priorSeniors, int currSeniors, int priorJuniors, int currJuniors) {

        int currentStaffCapacity = (seniorCapacity * currSeniors) + (juniorCapacity * currJuniors);
        int distance = Math.abs(currentStaffCapacity - buildingSize);

        if (distance <= minInterval && currentStaffCapacity >= buildingSize) {
            return new int[]{currSeniors, currJuniors};
        }

        if (distance > minStep && currentStaffCapacity < buildingSize) {
            return new int[]{priorSeniors, priorJuniors};
        }


        if (currentStaffCapacity > buildingSize && currSeniors > 1) {
            return exploreParams(distance, buildingSize, minInterval, minStep, seniorCapacity, juniorCapacity, currSeniors, currSeniors - 1, currJuniors, currJuniors);
        }

        if (currentStaffCapacity < buildingSize) {
            return exploreParams(priorDistance, buildingSize, minInterval, minStep, seniorCapacity, juniorCapacity, currSeniors, currSeniors, currJuniors, currJuniors + 1);
        }

        return new int[]{currSeniors, currJuniors};
    }
}
