package one.spo.coding.service;

import one.spo.coding.domain.CapacityPairResponse;

import java.util.List;

/**
 * User: raychenon
 */
public interface OptimizationService {

    public List<CapacityPairResponse> optimizeCapacity(List<Integer> rooms, int junior, int senior);
    
}
