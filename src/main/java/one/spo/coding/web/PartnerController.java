package one.spo.coding.web;

import one.spo.coding.domain.GsonUtils;
import one.spo.coding.domain.CapacityPairResponse;
import one.spo.coding.domain.WorkforceRequest;
import one.spo.coding.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * User: raychenon
 */
@RestController
public class PartnerController {

    private PartnerService partnerService;

    @Autowired
    public PartnerController(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody
    ModelAndView index() {
        return new ModelAndView("index");
    }


    @RequestMapping(value = "/optimization", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getByQueryParam(@RequestParam("rooms") List<Integer> rooms,
                   @RequestParam("junior") int capacityJunior,
                   @RequestParam("senior") int capacitySenior) {

        List<CapacityPairResponse> response = partnerService.optimizeCapacity(rooms, capacitySenior,capacityJunior);

        return GsonUtils.convertToJson(response);
    }


    @PostMapping(path = "/workforce", produces = "application/json")
    public @ResponseBody
    String postInputs(@RequestBody String json) {
        WorkforceRequest request = GsonUtils.fromRequest(json);
        List<CapacityPairResponse> response = partnerService.optimizeCapacity(request.getRooms(), request.getSenior(),request.getJunior());
        return GsonUtils.convertToJson(response);
    }


}
