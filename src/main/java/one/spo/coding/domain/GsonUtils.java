package one.spo.coding.domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;

/**
 * User: raychenon
 * Date: 8/2/19
 * Time: 12:38 AM
 */
public final class GsonUtils {


    private static final Gson GSON = createGson(true);

    private static final Gson createGson() {
        return createGson(true);
    }

    private static final Gson createGson(final boolean serializeNulls) {
        final GsonBuilder builder = new GsonBuilder();
//        builder.registerTypeAdapter(Date.class, new DateFormatter());
//        builder.registerTypeAdapter(Event.class, new EventFormatter());
        builder.setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES);
        if (serializeNulls)
            builder.serializeNulls();
        return builder.create();
    }


    public static String convertToJson(List<CapacityPairResponse> response) {
        String json = GSON.toJson(response);
        return json;
    }


    public static WorkforceRequest fromRequest(String json) {
        return GSON.fromJson(json, WorkforceRequest .class);
    }
}
