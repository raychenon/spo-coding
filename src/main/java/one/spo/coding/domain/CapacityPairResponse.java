package one.spo.coding.domain;

/**
 * User: raychenon
 */
public class CapacityPairResponse {
    private int senior;
    private int junior;

    public CapacityPairResponse(int senior, int junior) {
        this.senior = senior;
        this.junior = junior;
    }

    public int getSenior() {
        return senior;
    }

    public void setSenior(int senior) {
        this.senior = senior;
    }

    public int getJunior() {
        return junior;
    }

    public void setJunior(int junior) {
        this.junior = junior;
    }


}