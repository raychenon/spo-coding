package one.spo.coding.domain;

import java.util.List;

/**
 * User: raychenon
 */
public class WorkforceRequest {
    private List<Integer> rooms;
    private int senior;
    private int junior;

    public WorkforceRequest(List<Integer> rooms, int senior, int junior) {
        this.rooms = rooms;
        this.senior = senior;
        this.junior = junior;
    }

    public List<Integer> getRooms() {
        return rooms;
    }

    public void setRooms(List<Integer> rooms) {
        this.rooms = rooms;
    }

    public int getSenior() {
        return senior;
    }

    public void setSenior(int senior) {
        this.senior = senior;
    }

    public int getJunior() {
        return junior;
    }

    public void setJunior(int junior) {
        this.junior = junior;
    }

}
